//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Design3.Areas.SuperAdmin
{
    using System;
    using System.Collections.Generic;
    
    public partial class Locality
    {
        public int Id { get; set; }
        public Nullable<double> Latitude { get; set; }
        public Nullable<decimal> Longitude { get; set; }
        public System.Data.Spatial.DbGeography Geohash { get; set; }
    }
}
