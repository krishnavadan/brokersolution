﻿using System.Web.Mvc;

namespace Design3.Areas.superadmin
{
    public class SuperAdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SuperAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SuperAdmin_default",
                "SuperAdmin/{controller}/{action}/{id}",
                new { Controller = "SuperAdmin" , action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
