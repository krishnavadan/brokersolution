﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.SuperAdmin.Controllers
{
    public class StateController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /SuperAdmin/State/

        public ActionResult Index(string searchState=null)
        {
            var states = db.States.Include(s => s.Country);
            if (!string.IsNullOrEmpty(searchState))
            {
                states = states.Where(c => c.State_name.Contains(searchState));
            }
           
            return View(states.ToList());
        }

        //
        // GET: /SuperAdmin/State/Details/5

        public ActionResult Details(int id = 0)
        {
            State state = db.States.Find(id);
            if (state == null)
            {
                return HttpNotFound();
            }
            return View(state);
        }

        //
        // GET: /SuperAdmin/State/Create

        public ActionResult Create()
        {
            ViewBag.Country_id = new SelectList(db.Countries, "Id", "Country_name");
            return View();
        }

        //
        // POST: /SuperAdmin/State/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(State state)
        {
            if (ModelState.IsValid)
            {
                db.States.Add(state);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Country_id = new SelectList(db.Countries, "Id", "Country_name", state.Country_id);
            return View(state);
        }

        //
        // GET: /SuperAdmin/State/Edit/5

        public ActionResult Edit(int id = 0)
        {
            State state = db.States.Find(id);
            if (state == null)
            {
                return HttpNotFound();
            }
            ViewBag.Country_id = new SelectList(db.Countries, "Id", "Country_name", state.Country_id);
            return View(state);
        }

        //
        // POST: /SuperAdmin/State/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(State state)
        {
            if (ModelState.IsValid)
            {
                db.Entry(state).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Country_id = new SelectList(db.Countries, "Id", "Country_name", state.Country_id);
            return View(state);
        }

        //
        // GET: /SuperAdmin/State/Delete/5

        public ActionResult Delete(int id = 0)
        {
            State state = db.States.Find(id);
            if (state == null)
            {
                return HttpNotFound();
            }
            return View(state);
        }

        //
        // POST: /SuperAdmin/State/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            State state = db.States.Find(id);
            db.States.Remove(state);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}