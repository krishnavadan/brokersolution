﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.SuperAdmin.Controllers
{
    public class CItyController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /SuperAdmin/CIty/

        public ActionResult Index(string searchCity=null)
        {
            var cities = db.Cities.Include(c => c.State);
            if (!string.IsNullOrEmpty(searchCity))
            {
                cities = cities.Where(c => c.City_name.Contains(searchCity));
            }
            //var cities = db.Cities.Include(c => c.State);
            return View(cities.ToList());
        }

        //
        // GET: /SuperAdmin/CIty/Details/5

        public ActionResult Details(int id = 0)
        {
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        //
        // GET: /SuperAdmin/CIty/Create

        public ActionResult Create()
        {
            ViewBag.State_id = new SelectList(db.States, "Id", "State_name");
            return View();
        }

        //
        // POST: /SuperAdmin/CIty/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(City city)
        {
            if (ModelState.IsValid)
            {
                db.Cities.Add(city);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.State_id = new SelectList(db.States, "Id", "State_name", city.State_id);
            return View(city);
        }

        //
        // GET: /SuperAdmin/CIty/Edit/5

        public ActionResult Edit(int id = 0)
        {
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            ViewBag.State_id = new SelectList(db.States, "Id", "State_name", city.State_id);
            return View(city);
        }

        //
        // POST: /SuperAdmin/CIty/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(City city)
        {
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.State_id = new SelectList(db.States, "Id", "State_name", city.State_id);
            return View(city);
        }

        //
        // GET: /SuperAdmin/CIty/Delete/5

        public ActionResult Delete(int id = 0)
        {
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        //
        // POST: /SuperAdmin/CIty/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            City city = db.Cities.Find(id);
            db.Cities.Remove(city);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}