﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.SuperAdmin.Controllers
{
    public class LocalityController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /SuperAdmin/Locality/

        public ActionResult Index()
        {
            return View(db.Localities.ToList());
        }

        //
        // GET: /SuperAdmin/Locality/Details/5

        public ActionResult Details(int id = 0)
        {
            Locality locality = db.Localities.Find(id);
            if (locality == null)
            {
                return HttpNotFound();
            }
            return View(locality);
        }

        //
        // GET: /SuperAdmin/Locality/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SuperAdmin/Locality/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Locality locality)
        {
            if (ModelState.IsValid)
            {
                db.Localities.Add(locality);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(locality);
        }

        //
        // GET: /SuperAdmin/Locality/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Locality locality = db.Localities.Find(id);
            if (locality == null)
            {
                return HttpNotFound();
            }
            return View(locality);
        }

        //
        // POST: /SuperAdmin/Locality/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Locality locality)
        {
            if (ModelState.IsValid)
            {
                db.Entry(locality).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(locality);
        }

        //
        // GET: /SuperAdmin/Locality/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Locality locality = db.Localities.Find(id);
            if (locality == null)
            {
                return HttpNotFound();
            }
            return View(locality);
        }

        //
        // POST: /SuperAdmin/Locality/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Locality locality = db.Localities.Find(id);
            db.Localities.Remove(locality);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}