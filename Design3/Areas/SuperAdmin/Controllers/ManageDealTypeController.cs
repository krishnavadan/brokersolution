﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.SuperAdmin.Controllers
{
    public class ManageDealTypeController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /SuperAdmin/ManageDealType/

        public ActionResult Index(string searchDeal=null)
        {
            var Deals = db.Manage_Deal_Type.ToList();
            if (!string.IsNullOrEmpty(searchDeal))
            {
               // Deals = Deals.Where(c => c.Deal_type.Contains(searchDeal)).ToString();
            }
            return View(Deals);
        }

        //
        // GET: /SuperAdmin/ManageDealType/Details/5

        public ActionResult Details(int id = 0)
        {
            Manage_Deal_Type manage_deal_type = db.Manage_Deal_Type.Find(id);
            if (manage_deal_type == null)
            {
                return HttpNotFound();
            }
            return View(manage_deal_type);
        }

        //
        // GET: /SuperAdmin/ManageDealType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SuperAdmin/ManageDealType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Manage_Deal_Type manage_deal_type)
        {
            if (ModelState.IsValid)
            {
                db.Manage_Deal_Type.Add(manage_deal_type);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(manage_deal_type);
        }

        //
        // GET: /SuperAdmin/ManageDealType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Manage_Deal_Type manage_deal_type = db.Manage_Deal_Type.Find(id);
            if (manage_deal_type == null)
            {
                return HttpNotFound();
            }
            return View(manage_deal_type);
        }

        //
        // POST: /SuperAdmin/ManageDealType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Manage_Deal_Type manage_deal_type)
        {
            if (ModelState.IsValid)
            {
                db.Entry(manage_deal_type).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(manage_deal_type);
        }

        //
        // GET: /SuperAdmin/ManageDealType/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Manage_Deal_Type manage_deal_type = db.Manage_Deal_Type.Find(id);
            if (manage_deal_type == null)
            {
                return HttpNotFound();
            }
            return View(manage_deal_type);
        }

        //
        // POST: /SuperAdmin/ManageDealType/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Manage_Deal_Type manage_deal_type = db.Manage_Deal_Type.Find(id);
            db.Manage_Deal_Type.Remove(manage_deal_type);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}