﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.superadmin.Controllers
{
    public class SuperAdminController : Controller
    {
        //
        // GET: /SuperAdmin/SuperAdmin/
        [Authorize(Roles = "superadmin")]
        public ActionResult Index()
        {
            return View();
        }

    }
}
