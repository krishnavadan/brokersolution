﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.SuperAdmin.Controllers
{
    public class AbuseSupportController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /SuperAdmin/AbuseSupport/

        public ActionResult Index()
        {
            return View(db.AbuseSupports.ToList());
        }

        //
        // GET: /SuperAdmin/AbuseSupport/Details/5

        public ActionResult Details(int id = 0)
        {
            AbuseSupport abusesupport = db.AbuseSupports.Find(id);
            if (abusesupport == null)
            {
                return HttpNotFound();
            }
            return View(abusesupport);
        }

        //
        // GET: /SuperAdmin/AbuseSupport/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SuperAdmin/AbuseSupport/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AbuseSupport abusesupport)
        {
            if (ModelState.IsValid)
            {
                db.AbuseSupports.Add(abusesupport);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(abusesupport);
        }

        //
        // GET: /SuperAdmin/AbuseSupport/Edit/5

        public ActionResult Edit(int id = 0)
        {
            AbuseSupport abusesupport = db.AbuseSupports.Find(id);
            if (abusesupport == null)
            {
                return HttpNotFound();
            }
            return View(abusesupport);
        }

        //
        // POST: /SuperAdmin/AbuseSupport/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AbuseSupport abusesupport)
        {
            if (ModelState.IsValid)
            {
                db.Entry(abusesupport).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(abusesupport);
        }

        //
        // GET: /SuperAdmin/AbuseSupport/Delete/5

        public ActionResult Delete(int id = 0)
        {
            AbuseSupport abusesupport = db.AbuseSupports.Find(id);
            if (abusesupport == null)
            {
                return HttpNotFound();
            }
            return View(abusesupport);
        }

        //
        // POST: /SuperAdmin/AbuseSupport/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AbuseSupport abusesupport = db.AbuseSupports.Find(id);
            db.AbuseSupports.Remove(abusesupport);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}