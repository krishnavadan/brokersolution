﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.SuperAdmin.Controllers
{
    public class CompanyController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /SuperAdmin/Company/

        public ActionResult Index(string searchCompany = null)
        {
            var managecompanies = db.ManageCompanies.Include(m => m.City).Include(m => m.State);
            if (!string.IsNullOrEmpty(searchCompany))
            {
                managecompanies = managecompanies.Where(c => c.CompanyName.Contains(searchCompany));
            }
            return View(managecompanies.ToList());
        }

        //
        // GET: /SuperAdmin/Company/Details/5

        public ActionResult Details(int id = 0)
        {
            ManageCompany managecompany = db.ManageCompanies.Find(id);
            if (managecompany == null)
            {
                return HttpNotFound();
            }
            return View(managecompany);
        }

        //
        // GET: /SuperAdmin/Company/Create

        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.Cities, "Id", "City_name");
            ViewBag.StateId = new SelectList(db.States, "Id", "State_name");
            return View();
        }

        //
        // POST: /SuperAdmin/Company/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ManageCompany managecompany)
        {
            if (ModelState.IsValid)
            {
                db.ManageCompanies.Add(managecompany);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CityId = new SelectList(db.Cities, "Id", "City_name", managecompany.CityId);
            ViewBag.StateId = new SelectList(db.States, "Id", "State_name", managecompany.StateId);
            return View(managecompany);
        }

        //
        // GET: /SuperAdmin/Company/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ManageCompany managecompany = db.ManageCompanies.Find(id);
            if (managecompany == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.Cities, "Id", "City_name", managecompany.CityId);
            ViewBag.StateId = new SelectList(db.States, "Id", "State_name", managecompany.StateId);
            return View(managecompany);
        }

        //
        // POST: /SuperAdmin/Company/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ManageCompany managecompany)
        {
            if (ModelState.IsValid)
            {
                db.Entry(managecompany).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.Cities, "Id", "City_name", managecompany.CityId);
            ViewBag.StateId = new SelectList(db.States, "Id", "State_name", managecompany.StateId);
            return View(managecompany);
        }

        //
        // GET: /SuperAdmin/Company/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ManageCompany managecompany = db.ManageCompanies.Find(id);
            if (managecompany == null)
            {
                return HttpNotFound();
            }
            return View(managecompany);
        }

        //
        // POST: /SuperAdmin/Company/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ManageCompany managecompany = db.ManageCompanies.Find(id);
            db.ManageCompanies.Remove(managecompany);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}