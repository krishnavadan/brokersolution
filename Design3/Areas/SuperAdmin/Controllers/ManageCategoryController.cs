﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.SuperAdmin.Controllers
{
    public class ManageCategoryController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /SuperAdmin/ManageCategory/

        public ActionResult Index()
        {
            return View(db.Manage_Category.ToList());
        }

        //
        // GET: /SuperAdmin/ManageCategory/Details/5

        public ActionResult Details(int id = 0)
        {
            Manage_Category manage_category = db.Manage_Category.Find(id);
            if (manage_category == null)
            {
                return HttpNotFound();
            }
            return View(manage_category);
        }

        //
        // GET: /SuperAdmin/ManageCategory/Create

        public ActionResult Create()
        {
            //ViewBag.pcategory = db.Manage_Category.Where(c => c.Parent_category_id == 0).ToList();
            ViewBag.Parent_category_id = new SelectList(db.Manage_Category.Where(c => c.Parent_category_id == 0).ToList(), "Id", "Category_name");
            return View();
        }

        //
        // POST: /SuperAdmin/ManageCategory/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Manage_Category manage_category)
        {
            

            if (ModelState.IsValid)
            {
                db.Manage_Category.Add(manage_category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(manage_category);
        }

        //
        // GET: /SuperAdmin/ManageCategory/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Manage_Category manage_category = db.Manage_Category.Find(id);
            if (manage_category == null)
            {
                return HttpNotFound();
            }
            return View(manage_category);
        }

        //
        // POST: /SuperAdmin/ManageCategory/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Manage_Category manage_category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(manage_category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(manage_category);
        }

        //
        // GET: /SuperAdmin/ManageCategory/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Manage_Category manage_category = db.Manage_Category.Find(id);
            if (manage_category == null)
            {
                return HttpNotFound();
            }
            return View(manage_category);
        }

        //
        // POST: /SuperAdmin/ManageCategory/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Manage_Category manage_category = db.Manage_Category.Find(id);
            db.Manage_Category.Remove(manage_category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}