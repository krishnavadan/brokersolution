//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Design3.Areas.SuperAdmin
{
    using System;
    using System.Collections.Generic;
    
    public partial class ManageCompany
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string AuthPerson1 { get; set; }
        public string AuthPerson2 { get; set; }
        public string AuthPersonPhNo1 { get; set; }
        public string AuthPersonPhNo2 { get; set; }
        public string Email { get; set; }
        public string CompanyLogo { get; set; }
        public string CompanyBanner { get; set; }
        public Nullable<int> CityId { get; set; }
        public Nullable<int> StateId { get; set; }
        public string HomePhNo { get; set; }
        public string OfficePhNo { get; set; }
        public string Status { get; set; }
        public string CompanyName { get; set; }
    
        public virtual City City { get; set; }
        public virtual State State { get; set; }
    }
}
