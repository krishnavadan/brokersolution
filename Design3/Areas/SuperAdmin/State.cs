//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Design3.Areas.SuperAdmin
{
    using System;
    using System.Collections.Generic;
    
    public partial class State
    {
        public State()
        {
            this.Cities = new HashSet<City>();
            this.ManageCompanies = new HashSet<ManageCompany>();
        }
    
        public int Id { get; set; }
        public Nullable<int> Country_id { get; set; }
        public string State_name { get; set; }
    
        public virtual ICollection<City> Cities { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<ManageCompany> ManageCompanies { get; set; }
    }
}
