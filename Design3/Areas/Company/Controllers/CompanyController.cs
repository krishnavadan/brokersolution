﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Design3.Areas.Company.Controllers
{
    public class CompanyController : Controller
    {
        //
        // GET: /Company/Company/
         [Authorize(Roles = "company")]

        public ActionResult Index()
        {
            return View();
        }

    }
}
