﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Security;
using WebMatrix.WebData;
using Design3.Filters;

namespace Design3.Controllers
{
    [InitializeSimpleMembership]
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            if (User.IsInRole("superadmin"))
            {
                return RedirectToAction("Index", "superadmin/superadmin");
            }
            else if (User.IsInRole("company"))
            {
                return RedirectToAction("Index", "company/company");
            }
            else if (User.IsInRole("branch"))
            {
                return RedirectToAction("Index", "branch/branch");
            }
            else if (User.IsInRole("broker"))
            {
                return RedirectToAction("Index", "broker/home");
            }
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
